package msgpstore

import (
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"testing"
)

func testFile() *os.File {
	f, err := ioutil.TempFile(".", "msgp.")
	if err != nil {
		panic(err)
	}
	return f
}

func TestOpen(t *testing.T) {
	f := testFile()
	defer os.Remove(f.Name())
	ioutil.WriteFile(f.Name(), []byte{0x81, 0xa5, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0xa5, 0x77, 0x6f, 0x72, 0x6c, 0x64}, 0644)
	ks, err := Open(f.Name())
	if err != nil {
		t.Error(err)
	}
	if len(ks.Data) != 1 {
		t.Errorf("expected %d got %d", 1, len(ks.Data))
	}
	if world, ok := ks.Data["hello"]; !ok || string(world) != `world` {
		t.Errorf("expected %s got %s", "world", world)
	}
}

func TestRegex(t *testing.T) {
	f := testFile()
	defer os.Remove(f.Name())
	ks := new(MsgpStore)
	ks.Set("hello:1", "world1")
	ks.Set("hello:2", "world2")
	ks.Set("hello:3", "world3")
	ks.Set("world:1", "hello1")
	if len(ks.GetAll(regexp.MustCompile(`hello`))) != len(ks.Keys())-1 {
		t.Errorf("Problem getting all")
	}
	if len(ks.GetAll(nil)) != len(ks.Keys()) {
		t.Errorf("Problem getting all")
	}
}

func BenchmarkOpen100(b *testing.B) {
	f := testFile()
	defer os.Remove(f.Name())
	ks := new(MsgpStore)
	for i := 1; i < 100; i++ {
		ks.Set("hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	Save(ks, f.Name())

	var err error
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ks, err = Open(f.Name())
		if err != nil {
			panic(err)
		}
	}
	Save(ks, f.Name())
}

func BenchmarkOpen10000(b *testing.B) {
	f := testFile()
	defer os.Remove(f.Name())
	ks := new(MsgpStore)
	for i := 1; i < 10000; i++ {
		ks.Set("hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	Save(ks, f.Name())

	var err error
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ks, err = Open(f.Name())
		if err != nil {
			panic(err)
		}
	}
	Save(ks, f.Name())
}

func BenchmarkGet(b *testing.B) {
	ks := new(MsgpStore)
	err := ks.Set("human:1", "Human{\"Dante\", 5.4}")
	if err != nil {
		panic(err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var human string
		ks.Get("human:1", &human)
	}
}

func BenchmarkSet(b *testing.B) {
	ks := new(MsgpStore)
	b.ResetTimer()
	// set a key to any object you want
	for i := 0; i < b.N; i++ {
		err := ks.Set("human:"+strconv.Itoa(i), "Human{\"Dante\", 5.4}")
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkSave100(b *testing.B) {
	ks := new(MsgpStore)
	for i := 1; i < 100; i++ {
		ks.Set("hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Save(ks, "benchmark.msgp")
	}
}

func BenchmarkSave10000(b *testing.B) {
	ks := new(MsgpStore)
	for i := 1; i < 10000; i++ {
		ks.Set("hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Save(ks, "benchmark.msgp")
	}
}

func BenchmarkSaveGz100(b *testing.B) {
	ks := new(MsgpStore)
	for i := 1; i < 100; i++ {
		ks.Set("hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Save(ks, "benchmark.msgp.gz")
	}
}

func BenchmarkSaveGz10000(b *testing.B) {
	ks := new(MsgpStore)
	for i := 1; i < 10000; i++ {
		ks.Set("hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Save(ks, "benchmark.msgp.gz")
	}
}
