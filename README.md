# jsonstore  :convenience_store:

[![GoDoc](https://godoc.org/bitbucket.org/8ox86/msgpstore?status.svg)](https://godoc.org/bitbucket.org/8ox86/msgpstore)

*MsgpStore* is a Go-library for a simple thread-safe in-memory JSON key-store with persistent backend.
It's made for those times where you don't need a RDBMS like [MySQL](https://www.mysql.com/),
or a NoSQL like [MongoDB](https://www.mongodb.com/) - basically when you just need a simple keystore.
A really simple keystore. *MsgpStore* is used in those times you don't need a distributed keystore
like [etcd](https://coreos.com/etcd/docs/latest/), or
a remote keystore [Redis](https://redis.io/) or a local keystore
like [Bolt](https://github.com/boltdb/bolt).
Its really for those times where you just need a key-value store file.
only string data type supported.

## Usage

First, install the library using:

```
go get -u -v bitbucket.org/8ox86/msgpstore
```

Then you can add it to your program. Check out the examples, or see below for basic usage:

```golang
ks := new(jsonstore.MsgpStore)

err := ks.Set("human:1", "this is human 1")
if err != nil {
  panic(err)
}

// Saving will automatically gzip if .gz is provided
if err = msgpstore.Save(ks, "humans.json.gz"); err != nil {
  panic(err)
}

// Load any JSON / GZipped JSON
ks2, err := msgpstore.Open("humans.json.gz")
if err != nil {
  panic(err)
}

// get the data back via an interface
var human string
err = ks2.Get("human:1", &human)
if err != nil {
  panic(err)
}
fmt.Println(human) // Prints 'this is human 1'
```

# Dev

Benchmark against using Redis and BoltDB as KeyStores using Go1.10 (Intel i7-8700K CPU @ 4.50GHz).
Take away is that setting/getting is faster in *MsgpStore* (because its just a map),
but opening is much slower (because its a file that is read into memory). So don't use this if you have to store 1,000,000+ things!

```
BenchmarkOpen100-12                30000             43435 ns/op
BenchmarkOpen10000-12                300           4701279 ns/op
BenchmarkGet-12                 30000000                41.0 ns/op
BenchmarkSet-12                  3000000               382 ns/op
BenchmarkSave100-12                 5000            400946 ns/op
BenchmarkSave10000-12                 50          36788082 ns/op
BenchmarkSaveGz100-12              10000            241150 ns/op
BenchmarkSaveGz10000-12              100          17769683 ns/op
PASS
ok      bitbucket.org/8ox86/msgpstore   14.531s
```

```
BenchmarkOpen100-12        	   20000	     81664 ns/op
BenchmarkOpen10000-12      	     200	   9016223 ns/op
BenchmarkGet-12            	 2000000	       807 ns/op
BenchmarkSet-12            	 1000000	      1038 ns/op
BenchmarkSave100-12        	   20000	     97446 ns/op
BenchmarkSave10000-12      	     200	  31401238 ns/op
BenchmarkSaveGz100-12      	    5000	    250647 ns/op
BenchmarkSaveGz10000-12    	     100	  26312147 ns/op
PASS
ok  	_/home/hacklog/repo/jsonstore	22.814s
```

```
▶  cat bolt.txt
goos: linux
goarch: amd64
BenchmarkSet-12                     2000           1140323 ns/op
BenchmarkGet-12                  1000000              1300 ns/op
BenchmarkOpen100-12               200000             10652 ns/op
BenchmarkOpen10000-12             200000              9888 ns/op
PASS
ok      command-line-arguments  8.545s
```


```
$ go test -bench=. tests/redis/* > redis.txt
$ go test -bench=. tests/bolt/* > bolt.txt
$ go test -bench=. > msgpstore.txt
$ benchcmp bolt.txt msgpstore.txt
$ benchcmp redis.txt msgpstore.txt
```

# License

MIT
