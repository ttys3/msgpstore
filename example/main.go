package main

import (
	"fmt"
	"sync"
	"github.com/ihacklog/msgpstore"
	"os"
	"github.com/fsnotify/fsnotify"
	"time"
)

func main() {
	ks := new(msgpstore.MsgpStore)

	err := ks.Set("human:1", "this is human 1")
	if err != nil {
		panic(err)
	}

	// Saving will automatically gzip if .gz is provided,
	// and can be performed in a wait group
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err = msgpstore.Save(ks, "test.msgp"); err != nil {
			panic(err)
		}
	}()
	wg.Wait()

	// Load any JSON / GZipped JSON
	ks2, err := msgpstore.Open("test.msgp")
	if err != nil {
		panic(err)
	}
	defer os.Remove("test.msgp")

	ks2.WatchDb()
	ks2.OnDbChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})

	// get the data back via an interface
	var human string
	err = ks2.Get("human:1", &human)
	if err != nil {
		panic(err)
	}
	fmt.Println(human)
	for {
		time.Sleep(time.Second * 1)
	}
}
